import json

printDebug = False
indentLevel = 0
indent = "   "

errs = []

def printInd(s):
    if printDebug:
        print((indentLevel*indent) + s)

def typeDesc(thing):
    if printDebug:
        if type(thing) == dict:
            return "{"
        elif type(thing) == list:
            return "["
        elif type(thing) == str:
            return thing[:20] + ("..." if len(thing) > 20 else "")
        elif type(thing) == type(None):
            return "None"
        elif type(thing) in [int, bool]:
            return str(thing)
        else:
            return f"{type(thing)}"
    else:
        return None

def thingToObj(thing, name="<root>"):
    global indentLevel
    indentLevel += 1
    obj = thing

    if type(thing) == dict:
        obj = dictToObj(thing)
    elif type(thing) == list:
        obj = listToObj(thing)
    elif type(thing) in [str, int, bool, type(None)]:
        pass
    else:
        err = f"unknown type for {name}: {type(thing)}"
        printInd(err)
        errs.append(err)


    indentLevel -= 1
    return obj

def dictToObj(d):
    # global indentLevel

    o = lambda: None
    # indentLevel += 1
    for k in d.keys():
        printInd(f"{k} : {typeDesc(d[k])}")

        converted = thingToObj(d[k], k)
        setattr(o, k, converted)

    # indentLevel -= 1
    return o

def listToObj(orig):
    l = []

    for i in range(len(orig)):
        printInd(f"[{i}] : {typeDesc(orig[i])}")

        converted = thingToObj(orig[i], str(i))
        l.append(converted)

    return l


def convertJsonDict(json):
    return thingToObj(json)

if __name__ == "__main__":
    f = open("cached/posts-small.json")
    s = f.read()
    j = json.loads(s)

    o = convertJsonDict(j)

    print(o.data[0].attributes.content)

    if (len(errs) > 0):
        print("Found errors:")
        for err in errs:
            print(err)
    else:
        print("OK")