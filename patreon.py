from typing import List
import requests
from requests.models import Response

import json
import re

import jsonobj

DEBUGCACHE = False

cookieFile = open("cookie")
Cookie = cookieFile.read()
cookieFile.close()

context = {
    "headers": {
        "cookie": Cookie
    }
}

def patreonGet(context, url):
    r = requests.get(url, headers=context["headers"])

    print (f"GET {url} : {r.status_code}")
    ## TODO: do some general error detection and handling,
    ## such as for an expired session

    return r

def extractJson(rawhtml, objectName):
    jsonRE = r"Object.assign\(window.patreon." + objectName + r",(?P<json>.*?)\);"

    m = re.search(jsonRE, rawhtml, re.MULTILINE | re.DOTALL)
    if m is not None:
        jsontext = m.groupdict()["json"].strip()
        obj = json.loads(jsontext)

        return obj

    else:
        print("Could not find window.patreon." + objectName)

def cachedResponse(filename):
    print (f"using cached result from {filename}")
    f = open(filename)
    text = f.read()

    r = lambda:None
    setattr(r, "ok", True)
    setattr(r, "text", text)

    return r

###
#### Endpoint functions
###

def patreonPosts(context, campaignID, cursor=""):
    url=f"https://www.patreon.com/api/posts?include=campaign%2Caccess_rules%2Cattachments%2Caudio%2Cimages%2Cmedia%2Cpoll.choices%2Cpoll.current_user_responses.user%2Cpoll.current_user_responses.choice%2Cpoll.current_user_responses.poll%2Cuser%2Cuser_defined_tags%2Cti_checks&fields%5Bcampaign%5D=currency%2Cshow_audio_post_download_links%2Cavatar_photo_url%2Cearnings_visibility%2Cis_nsfw%2Cis_monthly%2Cname%2Curl&fields%5Bpost%5D=change_visibility_at%2Ccomment_count%2Ccontent%2Ccurrent_user_can_comment%2Ccurrent_user_can_delete%2Ccurrent_user_can_view%2Ccurrent_user_has_liked%2Cembed%2Cimage%2Cis_paid%2Clike_count%2Cmeta_image_url%2Cmin_cents_pledged_to_view%2Cpost_file%2Cpost_metadata%2Cpublished_at%2Cpatreon_url%2Cpost_type%2Cpledge_url%2Cthumbnail_url%2Cteaser_text%2Ctitle%2Cupgrade_url%2Curl%2Cwas_posted_by_campaign_owner%2Chas_ti_violation&fields%5Bpost_tag%5D=tag_type%2Cvalue&fields%5Buser%5D=image_url%2Cfull_name%2Curl&fields%5Baccess_rule%5D=access_rule_type%2Camount_cents&fields%5Bmedia%5D=id%2Cimage_urls%2Cdownload_url%2Cmetadata%2Cfile_name&filter%5Bcampaign_id%5D={campaignID}&filter%5Bcontains_exclusive_posts%5D=true&filter%5Bis_draft%5D=false&sort=-published_at&json-api-version=1.0&page%5Bcursor%5D={cursor}"

    if DEBUGCACHE:
        r = cachedResponse("cached/posts.json")
    else:
        r = patreonGet(context, url)

    if r.ok:
        ## This should already be a json string, just deserialize and return
        return json.loads(r.text)
    else:
        print ("post endpoint error: " + url)


def patreonPledge(context):
    baseurl = "https://www.patreon.com/pledges"

    r:Response = None

    if DEBUGCACHE:
        r = cachedResponse("cached/pledges.txt")
    else:  
        r = patreonGet(context, baseurl)

    if r.ok:
        bootstrap = extractJson(r.text, "bootstrap")
        c = extractJson(r.text, "campaignFeatures")

        return {
            "bootstrap": bootstrap,
            "c": c
        }
    else:
        print("Pledge endpoint error")


def test_posts():

    obj = patreonPosts(context, 
        ## 4951881 ## Dylan
        3876079 # Nommz
    )

    o = jsonobj.convertJsonDict(obj)

    print(o.data[0].attributes.content)

def test_pledge():
    obj = patreonPledge(context)

    bootstrap = obj["bootstrap"]
    c = obj["c"]

    incl:list[object] = bootstrap["cards"]["included"]
    campaigns = list(filter(lambda x: x["type"] == "campaign", incl))

    print (len(campaigns))

    for cmp in campaigns:
        name = cmp["attributes"]["name"]
        id = cmp["id"]
        url = f"https://www.patreon.com/api/campaigns/{id}"

        print (f"CMP {id} - {name} - {url}")
        # print("cmp")

    print (c)


if __name__ == "__main__":
    test_posts()