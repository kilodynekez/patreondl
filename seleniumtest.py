#from ipdb import set_trace
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL= 'https://www.patreon.com/'
username = ''
password = ''

driver = webdriver.Firefox(executable_path="./geckodriver.exe")
driver.set_window_size(1024, 768) # optional

# login
driver.get(URL + 'login')
driver.find_element(By.CSS_SELECTOR, 'input[aria-label="Email"]').send_keys(username)
driver.find_element(By.NAME, 'password').send_keys(password)
driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]').click()

# wait for page to load
WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.CSS_SELECTOR, 'div[data-tag="post-card"]'))
)

#ipdb.set_trace()

# grab the links
posts = driver.find_elements(By.CSS_SELECTOR, 'div[data-tag="post-card"]')
# print the urls
# print([a.get_attribute("src") for a in audios])
print (posts)