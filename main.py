import requests
import os, json, sys, traceback
import subprocess
import mimetypes
import patreon
import jsonobj
import re
import time

#DL_DIR = "dl"
DL_DIR = "G:\\porn\\patreon"
campaignFilepath = os.path.join(DL_DIR, "campaigns.json")

DL_POST_LIMIT = 4

DISPO_SUCCESS = "success"
DISPO_PARTIAL = "partial"
DISPO_ERROR = "error"
DISPO_RESTRICTED = "restricted"

ignoredTypes = [
    "user",
    "campaign",
    "post_tag",
    "access-rule" ,
    "reward",
    "goal",
    "poll",
    "poll_choice"
]

processTypes = [
    "media",
    # "attachment"
]

errors = {}
warnings = []

info = {}

# def warn(warning:str):
#     warnings.append(warning)

def error(err:str, pid=None):
    pid = pid if pid is not None else "general"
    if pid not in errors:
        errors[pid] = []

    errors[pid].append(err)

def dispositionPost(p, record, runstamp, status):
    print(f"\tdispositioning as {status['status']}")

    disp = record["meta"]["dispositions"]
    pid = getId(p)

    if pid not in disp:
        disp[pid] = {}
        disp[pid]["history"] = {}

    ## Add this run to the history
    disp[pid]["history"][runstamp] = status

    ## Keep given status as the "current" status if no current status exists,
    ## or if the new status is "better" than the old one
    if "current" in disp[pid]:
        current = disp[pid]["current"]
        if current["status"] == DISPO_SUCCESS:
            ## Ignore all new statuses
            pass
        elif current["status"] == DISPO_ERROR:
            ## Overwrite with new in all cases (incl new error)
            disp[pid]["current"] = status
        elif current["status"] == DISPO_PARTIAL:
            ## if current is partial, only overwrite if new is success or partial
            if status["status"] in [DISPO_SUCCESS, DISPO_PARTIAL]:
                disp[pid]["current"] = status
        elif current["status"] == DISPO_RESTRICTED:
            ## if current is restricted, overwrite in all cases
            disp[pid]["current"] = status
    else:
        disp[pid]["current"] = status



## TODO: redo this function so that it checks all
##       runs and returns the most-recent one
##
##  ALSO: how do we quickly get a status on all the
##        successfull posts so we know we're okay to skip them?
##        Checking disposition on every post seems like a lot,
##         especially if we have to check through multiple runs...
def getLatestPostDisposition(record, pid, disprun):
    disp = record["meta"]["dispositions"]
    if len(disp) == 0:
        return None  ## If no runs exist, return None

    ## Get latest runstamp
    runstamp = sorted(disp.keys())[-1] 
    disprun = disp[runstamp]

    ## Attempt to find the status in this run
    #### NOTE: unfinished!!
    raise NotImplementedError()




def processPost(p, postj, record, included_dict, runstamp):
    pid = getId(p)
    try:

        title = p.attributes.title
        typ = p.attributes.post_type
        sys.stdout.write(f"{pid} - {title} - {typ}")

        ## Get current latest disposition of this post to see if
        ## we need to attempt to process it
        disp = record["meta"]["dispositions"]
        if pid in disp and "current" in disp[pid]:
            current = disp[pid]["current"]

            if current["status"] == DISPO_SUCCESS:
                sys.stdout.write("\n\tAlready processed successfully - skipping\n")
                return


        prefix = f"{p.id}_"
        campDir = getCampDir(record)

        content = p.attributes.content
        embed = p.attributes.embed
        assoc_media_ids = [getId(item) for item in p.relationships.media.data]
        assoc_attach_ids = [getId(item) for item in p.relationships.attachments.data]
        ## TODO: also grab audio, attachments ids

        sys.stdout.write(f" - {assoc_media_ids}")

        ## Gather metadata about post types
        if "post_types" not in info:
            info["post_types"] = {}

        type_key = "None" if typ is None else typ
        if type_key not in info["post_types"]:
            info["post_types"][type_key] = 0
        info["post_types"][type_key] += 1

        ## TODO: check "post_type" = "video_embed", then ["embed"]["url"] 
        if typ == "video_embed":
            downloadEmbeddedVideo(p, campDir, prefix)


        ## TODO: How to handle posts that user doesn't have access to (higher tier)?
        ##   Skipping them would throw off processed count, /but/ they haven't
        ##   really been backed up (e.g. what if I sub at a higher tier later?)
        ##   Should probably back them up, but with some tag to denote that they
        ##   were not fully archived because of the tier.  May need to have a 
        ##   campaign level note of this, so that we can easily check it and 
        ##   re-archive if the tier level changes.
        ##   Of course, only re-archive content that was skipped -- don't want
        ##   to delete old content if we've moved DOWN a tier.

        ## TODO: Stop paging if the number of processed posts equals
        ##       the tottal number of posts

    
        ## Download files 
        sys.stdout.write(" Processing Media....\n")
        downloadFilesWithIds(assoc_media_ids, included_dict, campDir, prefix, pid)
        sys.stdout.write(" Processing Attachments....\n")
        downloadFilesWithIds(assoc_attach_ids, included_dict, campDir, prefix, pid)

        ## Record post data in the record
        record["data"][pid] = postj

        ## Check for errors for this post.  If we got to this point, but errors
        ## were found, we will call this a partial success.
        postErrors = errors[pid] if pid in errors else None
        if postErrors is not None:
            ## Disposition post as a partial success
            dispositionPost(p, record, runstamp, {
                "status": DISPO_PARTIAL,
                "errors": postErrors
            })
        else:
            ## Disposition post as success
            dispositionPost(p, record, runstamp, {
                "status": DISPO_SUCCESS
            })

    except Exception as e:
        exinfo = traceback.format_exc()
        error(exinfo, pid)

        postErrors = errors[pid]
        dispositionPost(p, record, runstamp, {
            "status": DISPO_ERROR,
            "errors": postErrors,
            "postdata": postj
        })


def scrapePosts(campaignIDRaw, record, runstamp, cursor=""):
    ## NOTE: Debug cached hardcoded
    postsj = patreon.patreonPosts(patreon.context, campaignIDRaw, cursor)
    posts = jsonobj.convertJsonDict(postsj)

    print (f"Gathered {len(posts.data)} posts!")

    ## Init record if needed.  This creates directories and files for storing
    ## the data for this campaign, and initializes some record data
    if (record["meta"]["campDir"] is None):
        initRecord(record, posts, postsj, campaignIDRaw)

    ## See if we need to process any more posts
    numProcessed = record["meta"]["numProcessed"]
    if numProcessed < posts.meta.pagination.total:
        if (record["meta"]["numPosts"] != posts.meta.pagination.total):
            print ("New posts have been added! (or deleted?)")
        else:
            print ("No new posts, but looks like we're not backed up fully")
        pass
    elif numProcessed == posts.meta.pagination.total:
        ## Looks like we're up to date!
        ## TODO: but we should verify that all post ids exist anyway
        ##  -- go through loop as normal (?) and exit only if no new ones were added
        print ("Already up to date!")
        return None
    else:
        ## Something weird has happened - posts have been deleted?
        print ("Weird, we've processed more posts than there are!")
        print ("We will need to re-adjust the processed number so as not to include the new deleted post?")
        print ("Or have some mechanism for not including it in the comparison count??")
        raise Exception("Deleted Posts Detected")

    ## Update total number of posts
    record["numPosts"] = posts.meta.pagination.total


    ## Add all included resources to the record immediately
    for inc in postsj["included"]:
        id = getId(inc)
        print(f"\tIncluding resource {id}")
        record["included"][id] = inc

    ## Preprocess `included` to be acessible by id 
    included_dict = {}
    for i in postsj["included"]:
        included_dict[getId(i)] = i

    ## Scrape posts and download content
    for i in range(len(posts.data)):
        processPost(posts.data[i], postsj["data"][i], record, included_dict, runstamp)


    ## Print some metadata about how many posts we've processed so far
    numProcessed = len(record["data"])
    record["meta"]["numProcessed"] = numProcessed
    numTotal = posts.meta.pagination.total
    print (f"Processed {numProcessed} / {numTotal} posts")

    try:
        ## If another cursor exists, print and return it
        print(posts.meta.pagination.cursors.next)
        return posts.meta.pagination.cursors.next
    except:
        ## Otherwise return None
        return None

## Download a video embedded at the given url using yt_dlp
def downloadVideo(url, path, nameTemplate):

    ret = subprocess.run(["python", "-m", "yt_dlp", 
            "-P", path,
            "-o", nameTemplate, 
            url],
        capture_output = True,
        encoding="UTF-8"
    )

    pathname = parseVideoFilePathname(ret)

    ## Check for a repeated extension (e.g. "video.mp4.mp4")
    ext = pathname.split(".")[-1]
    if pathname.endswith(f".{ext}.{ext}"):
        print ("Duplicate extension found, fixing...")
        newpathname = ".".join(pathname.split(".")[:-1])  # remove the last extension

        ## rename file to remove repeated extension
        try:
            os.rename(pathname, newpathname)
        except FileExistsError as e:
            ## If the destination already exists, then we must have already downloaded this before.
            ## Emit a warning and delete the new downloaded file
            print ("Already exists!")
            os.remove(pathname)
            pass


    return ret


def parseVideoFilePathname(completedProcess):
    stdout = completedProcess.stdout
    pathname = None

    m = re.search(r"\[download\] (?:Destination: (.*)\n\n|(.*) has already been downloaded)", stdout)
    if m is not None:
        pathname = m.group(1) or m.group(2)
        print ("filename found:")
        print(pathname)
    else:
        print ("no filename found in stdout")
        print(stdout)

    return pathname


def downloadEmbeddedVideo(post, dir, prefix):
    embed = post.attributes.embed
    pid = getId(post)

    filenameTemplate = f"{prefix}{embed.subject}.%(ext)s"
    # pathnameTemplate = os.path.join(dir, filenameTemplate)

    if embed.provider == "YouTube":

        # ret = subprocess.run(["python", "-m", "yt_dlp", "-o", pathnameTemplate, embed.url],
        #     capture_output = True,
        #     encoding="UTF-8"
        # )
        ret = downloadVideo(embed.url, dir, filenameTemplate)

        if ret.returncode != 0:
            error(f"Some error occurred with yt_dlp: (${ret.returncode})\n${ret.stdout}\n${ret.stderr}", pid)

        print (ret.stdout)

        if ret.stderr is not None and len(ret.stderr) > 0:
            error(ret.stderr, pid)
    ## TODO elif embed.provider == "Twitter"
    ## use twitter API to download mp4 from linked tweet
    ## https://stackoverflow.com/questions/32145166/get-video-from-tweet-using-twitter-api
    ## Check -- does yt-dlp work here?
    else:
        error(f"post {post.id}: Unknown embedded video provider: {embed.provider}", pid)
        return


def downloadFilesWithIds(ids, included_dict, dir, prefix="", pid=None):
    ## Download files
    ## TODO: rewrite this to handle other types
    for id in ids:
        try:
            notes = []
            sys.stdout.write(f"\t{id}")

            incl = included_dict[id]

            typ = incl["type"]
            filename = ""
            url = ""

            if typ == "media":
                filename = incl["attributes"]["file_name"]
                url = incl["attributes"]["download_url"]
            elif typ == "attachment":
                filename = incl["attributes"]["name"]
                url = incl["attributes"]["url"]
            else:
                sys.stdout.write(f"Don't know how to process type {typ} !")
                err = f"Unknown type: {id} {type}"
                error(err, pid)
                continue

            sys.stdout.write(f" - {filename} .... ")

            rawid = splitId(id)[1]

            ## Download files to campaign dir
            ## TODO: separate into folders based on type? (e.g. media, attachment?)
            ##  No, separating by attachment is dumb and hard to do a better way
            ## TODO: Refactor filenames?

            ## Verify that filename is valid
            if filename.startswith(("http://", "https://")):
                filename = "embed"
                notes.append(f"Renamed to {filename}")

            localFilename = f"{prefix}{rawid}_{filename}"
            localFilepath = os.path.join(dir, localFilename)

            ## Only download if file doesn't already exist
            if not os.path.exists(localFilepath):
                download_file(url, localFilepath, patreon.context["headers"])
                sys.stdout.write("DONE!\n")
            else:
                sys.stdout.write("Already exists!\n")

            ## Print out any non-error notes about this item
            for note in notes:
                print (f"\t\t{note}")
        except:
            print (f"Error downloading included resource: {id}")
            traceback.print_exc()
            error(traceback.format_exc(), pid)




def download_file(url, filename, headers):
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True, headers=headers) as r:
        ## If no extension is given by the patreon filename, add an extension based
        ## on the content type of the reply
        if re.search(r"\.\w{3,4}$", filename) is None:
            contentType = r.headers["Content-Type"]
            print(f"Content-Type: {contentType}")
            ext = mimetypes.guess_extension(contentType)
            print(f"ext: {ext}")
            filename += ext

        r.raise_for_status()
        with open(filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=16*1024*1024): 
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                #if chunk: 
                f.write(chunk)

def downloadAllForCampaign(campaignIdRaw, runstamp, maxPages=None):
    record = loadRecord(campaignIdRaw)

    try:
        nextCursor = ""
        pages = 0
        while (nextCursor is not None) and (maxPages == None or pages < maxPages):
            nextCursor = scrapePosts(campaignIdRaw, record, runstamp, nextCursor)
            pages += 1
    except Exception as e:
        traceback.print_exc()
    finally:
        print("persisting record...")
        persistRecord(record)
        print("Done!")

def getCampDir(obj, campaignId=None):
    campDir = None

    if campaignId is not None:
        ## Try to treat it like a campaign object from campaigns.json
        campDir = obj["meta"]["dir"][campaignId]
    else:
        ## Try to treat it like a record object from a records.json
        campDir = obj["meta"]["campDir"]

    return os.path.join(DL_DIR, campDir)

LATEST_RECORD_VERSION = 1

def newRecord():
    return {
        ## NOTE: Rather than being a lists, `data` and `included` 
        ##       are dictionaries mapping ids to objects
        "data": {},
        "included": {},
        "meta": {
            "version": LATEST_RECORD_VERSION,
            "dispositions": {},
            "numProcessed": 0,
            "numPosts": None,
            "campaignId": None,
            "campDir": None,
            "indexed": False   ## Whether this has been indexed in the root level campaigns.json
        }
    }

## Successively updates record to latest version
def updateRecord(record):
    version = record["meta"]["version"] if "version" in record["meta"] else 0
    if (version == LATEST_RECORD_VERSION):
        return record
    
    if version == 0:
        record["meta"]["dispositions"] = {} 
        version += 1  ## Upgrade to version 1
    
    # if version == 1:
    #     ## update from version 1 to 2
    #     version += 1  ## Upgrade to version 2

    record["meta"]["version"] = version
    return record


def loadRecord(campaignIdRaw):
    try:
        campaignId = getId({"type": "campaign", "id": campaignIdRaw})
        campaigns = loadIndex()
        # campDir = campaigns["meta"]["dir"][campaignId]
        campDir = getCampDir(campaigns, campaignId)

        recordFilepath = os.path.join(campDir, "record.json")
        recordFile = open(recordFilepath, "r")
        record = json.load(recordFile)
        recordFile.close()

        ## Update record version if needed
        updateRecord(record)
        pass

        return record
    except Exception as e:
        exinfo = traceback.format_exc()
        print(exinfo)
        error(exinfo)

        return newRecord()

def initRecord(record, posts, postsj, campaignIdRaw):
    ## Add campaign info
    ## Get campaign name and make dir
    camp = [i for i in posts.included if i.type == "campaign" and i.id == str(campaignIdRaw)]
    assert(len(camp) == 1)
    camp = camp[0]
    campName = camp.attributes.name.strip()

    campDir = os.path.join(DL_DIR, campName)
    os.makedirs(campDir, exist_ok=True)
    # postDir = os.path.join(campDir, "posts")
    # os.makedirs(postDir, exist_ok=True)

    campId = getId(camp)

    record["meta"]["campDir"] = campName
    record["meta"]["campaignId"] = campId
    record["meta"]["numPosts"] = posts.meta.pagination.total

    ## Persist record in campaigns.json index
    campj = [i for i in postsj["included"] if i["type"] == "campaign"]
    record["meta"]["indexed"] = True

    campaigns = loadIndex()
    campaigns["data"][campId] = campj
    campaigns["meta"]["dir"][campId] = campName

    persistIndex(campaigns)

def persistRecord(record):
    # campDir = record["meta"]["campDir"]
    campDir = getCampDir(record)
    campaignId = record["meta"]["campaignId"]

    recordFilename = f"record.json"
    recordFilepath = os.path.join(campDir, recordFilename)
    recordFile = open(recordFilepath, "w")
    json.dump(record, recordFile)
    recordFile.close()
    

def initIndex():
    return {
        "data": {},    ## Map of campaignId -> object
        "meta": {
            "dir": {}  ## Map of campaignId -> directory path 
        }
    }

def persistIndex(campaigns):
    campaignsFile = open(campaignFilepath, "w")
    json.dump(campaigns, campaignsFile)
    campaignsFile.close()

def loadIndex():
    try:
        campaignsFile = open(campaignFilepath, "r")
        campaigns = json.load(campaignsFile)
        campaignsFile.close()
        return campaigns
    except OSError: 
        ## File does not exist yet, create new one
        ## Later when this is persisted, the file will be created
        return initIndex()

def getId(item, typ=None):
    try:
        return joinId(item.type, item.id)
    except:
        return joinId(item["type"], item["id"])

def splitId(id):
    parts = id.split("-")
    return "-".join(parts[:-1]), parts[-1]

def joinId(typ, id):
    return f"{typ}-{id}"


def main():
    ids = [
        # 3006009,  # Joshii
        # 4951881,  # Dylan
        2223077,  #Thais
        # 4066709, ## Nikemd
        # 3876079 # Nommz

    ]
    # maxPages = 1
    maxPages = None

    runstamp = int(time.time())

    for id in ids:
        downloadAllForCampaign(id, runstamp, maxPages)

    if "post_types" in info:
        print ("post types")
        for infokey in info["post_types"]:
            print(infokey, " : ", info["post_types"][infokey])

    if (len(warnings) > 0):
        print("Warnings found!")
        for warning in warnings:
            print (warning)

    if (len(errors) > 0):
        print("Errors found!")
        for errtype in errors:
            print(f"\t{errtype}")
            for err in errors[errtype]:
                print(f"\t\t{err}")

    print ("Done!")

def an():
    ids = [
        # 3006009,  # Joshii
        # 4951881,  # Dylan
        # 2223077,  #Thais
    ]

    for id in ids:
        # idx = loadIndex()
        # campId = joinId("campaign", id)
        # camp = jsonobj.convertJsonDict(idx["data"][campId])

        # print(f"{camp.attributes.name}")

        r = loadRecord(id)
        camp = [inc for inc in r["included"].values() if inc["type"] == "campaign"][0]
        print(camp["attributes"]["name"])

        attachments = []

        typecount = {}        
        for iid in r["included"]:
            inc = r["included"][iid]
            if inc["type"] in ignoredTypes:
                continue

            ## Add to type dict
            if inc["type"] not in typecount:
                typecount[inc["type"]] = 0
            typecount[inc["type"]] += 1

            ## Do attachments
            if inc["type"] == "attachment":
                attachments.append(iid)


        ## Print type dict
        for t in typecount:
            print(f"\t{t}: {typecount[t]}")

        ## dl attachments
        downloadFilesWithIds(attachments, r["included"], getCampDir(r), prefix="attachment_")

def vidtest():
    ret = downloadVideo(
        # "https://drive.google.com/file/d/10hvMuIOAJ0bYHyT84Q6s9_jqIL140z6_/view?usp=sharing",
        "https://vimeo.com/610146318/aea24ce44f",
        "tmp",
        "tmp2_%(title)s.%(ext)s"
    )

    f = parseVideoFilePathname(ret)

    print (f)

if __name__ == "__main__":
    # vidtest()
    main()
    # an()
